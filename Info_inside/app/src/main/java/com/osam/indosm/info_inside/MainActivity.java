package com.osam.indosm.info_inside;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.osam.indosm.info_inside.Module.*;
import android.content.ServiceConnection;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import android.util.Log;

import android.net.Uri;

public class MainActivity extends Activity implements OnClickListener {

    private static String rank;
    private static String name;
    private static boolean intoWork=false; //True if you are in Working
    private static boolean intoLocation=false;
    private String QRsend; //Sending {rank : ~~,  name : ~~, UUID : ~~, Time : ~~, intoWork : ~~}
    private String QRread; //Receiving { intoWork : ~~, key : ~~, lati : ~~, logi : ~~, radi : }
    private TextView txtWorking;
    private TextView txtGPS;
    private ImageView ImgQr;
    private final int REQUEST_CAMERA = 9000;
    public Uri imageUri;
    private static double recvlati;
    private static double recvlogi;
    private static double recvradi;
    private static String privateKey;
    GpsInfo gps = null;
    int startnum;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.main_layout);

        SharedPreferences pref = getSharedPreferences("isSetup", Activity.MODE_PRIVATE);
        rank = pref.getString("rank", "");
        name = pref.getString("name", "");

        txtWorking = findViewById(R.id.txtWorking);
        txtGPS = findViewById(R.id.txtGPS);
        ImgQr = this.findViewById(R.id.ImgQr);

        findViewById(R.id.btnHome).setOnClickListener(this);
        findViewById(R.id.btnGallery).setOnClickListener(this);
        findViewById(R.id.btnSetup).setOnClickListener(this);

        gps = new GpsInfo(MainActivity.this);

    }
    @Override
    public void onResume() {
        super.onResume();
        String time = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        if (gps == null) {
            gps = new GpsInfo(MainActivity.this);
        } else {
            gps.Update();
        }
        if (gps.isGetLocation()) {
            // check if GPS enabled
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
        if(intoWork){
            if((gps.getLatitude() >= (recvlati-recvradi)) && (gps.getLatitude() <= (recvlati+recvradi))){
                if((gps.getLongitude() >= (recvlogi-recvradi))&&(gps.getLongitude() <= (recvlogi+recvradi))){
                    intoLocation=true;
                }
                else intoLocation=false;
            }
            else intoLocation=false;
        }
        if(intoWork){
            txtWorking.setText("Now i am working");
            txtWorking.setTextColor(Color.BLACK);
            if(intoLocation) {
                txtGPS.setText("And also I am in Office");
                txtGPS.setTextColor(Color.BLACK);
            }
            else{
                txtGPS.setText("But I am not in Office");
                txtGPS.setTextColor(Color.RED);
            }
        }
        else{
            txtWorking.setText("I am not Working Now");
            txtGPS.setText("");
            txtWorking.setTextColor(Color.RED);
        }
        QRsend = "{rank : " + rank + ", name : " + name + ", UUID : " + getDevicesUUID(this) +", Time : " + time + ", intoWork : "+ intoWork + "}";
        ImgQr.setImageBitmap(QRFeature.getQRCodeImage(QRsend, 300, 300));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnGallery) {
            Intent intent = new Intent(this, GalleryActivity.class);
            if(intoWork){
                intent.putExtra("intoWork", intoWork);
                intent.putExtra("intoLocation",intoLocation);
                intent.putExtra("Startnum", startnum);
            }
            startActivity(intent);
        }
        if (v.getId() == R.id.btnSetup) {
            // TODO add Advanced Setup Activity
        }
        if (v.getId() == R.id.btnHome) {
            IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
            integrator.setCaptureActivity(CustomScannerActivity.class);
            integrator.initiateScan();
        }
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    public void makeNewGpsService() {
        if(gps == null) {
            gps = new GpsInfo(MainActivity.this);
        }else{
            gps.Update();
        }

    }

    private String getDevicesUUID(Context mContext) {
        final TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        return deviceId;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == Activity.RESULT_OK) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            QRread = scanResult.getContents();
            checkQR(QRread.substring(1, QRread.length() - 1));
        }
    }

    public boolean isinlocation(){
        return this.intoLocation;
    }
    private void checkQR(final String QRMessage) {
        Log.e("Main", QRMessage);  //Receiving { intoWork : ~~, key : ~~, lati : ~~, logi : ~~, radi : }
        String Office_info[] = {"","","","",""}; // intoWork, key, lati, logi, radi;
        for(int i=0;i<5;i++){
            Office_info[i] = QRMessage.split(",")[i].split(": ")[1];
        }
        if(Office_info[0]!=String.valueOf(intoWork)){
            //TODO error messaging handle
        }
        intoWork = !intoWork;
        privateKey = Office_info[1];
        recvlati = Double.parseDouble(Office_info[2]);
        recvlogi = Double.parseDouble(Office_info[3]);
        recvradi = Double.parseDouble(Office_info[4]);

        if(intoWork) {
            Intent intent = new Intent(getApplicationContext(),PhotoFilter.class);
            startnum = getRecentFilenum();
            startService(intent);
        }
        else{
            Intent intent = new Intent(getApplicationContext(),PhotoFilter.class);
            stopService(intent);
            privateKey = "";
            recvlati = 0;
            recvlogi = 0;
            recvradi = 0;
        }
    }

    private int getRecentFilenum(){
        File[] listFiles = (new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera/").listFiles());
        int num = listFiles.length-1;
        if(listFiles[num].getName().endsWith("jpg") || listFiles[num].getName().endsWith(".bmp")){
            return num;
        }
        else{
            return 0;
        }
    }
}


