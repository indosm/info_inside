package com.osam.indosm.info_manager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.osam.indosm.info_manager.Module.QRFeature;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity {
    private final long TimeLimit = 5; // QRcode gets 5minutes validate
    boolean isClicked = false;
    private FirebaseFirestore db;
    private TextView txtWelcome;
    private Button btnOK;
    private ImageView imgQr;
    private String QRread; //Receiving {rank : ~~,  name : ~~, UUID : ~~, Time : ~~, intoWork : ~~}
    private String QRsend; //Sending { intoWork : ~~, key : ~~, lati : ~~, logi : ~~, radi : }
    private String privatekey = "!@#$";
    private double setlati = 36.307774, setlogi = 127.2235233, setradi = 0.05;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);
        txtWelcome = findViewById(R.id.txtWelcome);
        imgQr = findViewById(R.id.imgQr);
    }

    protected void onResume() {
        super.onResume();
        Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isClicked) {
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setCaptureActivity(CustomScannerActivity.class);
                    integrator.initiateScan();
                }
                isClicked = false;
            }
        }, 10000);
    }

    public void btnAccept(View v) {
        isClicked = true;
        IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
        integrator.setCaptureActivity(CustomScannerActivity.class);
        integrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == Activity.RESULT_OK) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            QRread = scanResult.getContents();
            checkID(QRread.substring(1, QRread.length() - 1));
        }
    }

    private void checkID(final String QRMessage) {
        Log.d("Main", QRMessage);
        final User user = new User(QRMessage);
        if (!checkTime(user.Gettime(), getCurrentTime())) {
            Log.d("main", "invalid QR Code - " + user.Gettime() + "///" + getCurrentTime());
            txtWelcome.setText("Invalide QR Code");
        } else {
            DocumentReference docRef = db.collection("Certificate User").document(user.GetUUID());
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        Map<String, Object> putdata = new HashMap<>();
                        if (document.exists()) {
                            if (user.GetintoWork().equals("false")) {
                                putdata.put(user.Gettime(), "go_in");
                            } else {
                                putdata.put(user.Gettime(), "go_out");
                            }
                            db.collection(user.GetUUID())
                                    .add(putdata)
                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(DocumentReference documentReference) {
                                            //Weldone
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            //failed to add
                                        }
                                    });
                            QRsend = "{intoWork : " + user.GetintoWork() + ", key : " + privatekey + ", lati : " + setlati + ", logi : " + setlogi + ", radi : " + setradi + "}";
                            imgQr.setImageBitmap(QRFeature.getQRCodeImage(QRsend, 300, 300));
                            txtWelcome.setText("Welcome\n" + user.Getname());
                        } else {
                            putdata.put("rank", user.Getrank());
                            putdata.put("name", user.Getname());
                            putdata.put("UUID", user.GetUUID());

                            Map<String, Object> putdata2 = new HashMap<>();
                            putdata2.put(user.Gettime(), putdata);
                            db.collection("Tryouts")
                                    .add(putdata2)
                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(DocumentReference documentReference) {
                                            Log.d("Main", "DocumentSnapshot added with ID: " + documentReference.getId());
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w("Main", "Error adding document", e);
                                        }
                                    });
                            imgQr.setImageResource(R.drawable.nop2);
                            txtWelcome.setText("You are not\nCertificated User");
                        }
                    } else {
                        imgQr.setImageResource(R.drawable.nop2);
                        txtWelcome.setText("Get failed to\n Connect Database");
                    }
                }
            });
        }
    }

    private String getCurrentTime() {
        String time = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        return time;
    }

    private boolean checkTime(String A, String B) {
        try {
            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date day1 = f.parse(A);
            Date day2 = f.parse(B);
            long diff = day1.getTime() - day2.getTime();
            long min = diff / 60000;
            if (min <= TimeLimit && min >= ((-1) * (TimeLimit))) return true;
            return false;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
