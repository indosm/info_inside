package com.osam.indosm.info_inside;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SetupActivity extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.setup_layout);

        final SharedPreferences pref = getSharedPreferences("isSetup", Activity.MODE_PRIVATE);
        boolean isSetup = pref.getBoolean("isSetup",false);
        if(isSetup){
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }
        else{
            //is Setuped
            Button btnRegist = findViewById(R.id.btnRegist);
            final TextView txtRank = findViewById(R.id.txtRank);
            final TextView txtName = findViewById(R.id.txtName);

            btnRegist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("rank", txtRank.getText().toString());
                    editor.putString("name", txtName.getText().toString());
                    editor.putBoolean("isSetup",true);
                    editor.commit();
                    startActivity(new Intent(SetupActivity.this, MainActivity.class));
                    finish();
                }
            });
        }
    }
}
