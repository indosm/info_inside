package com.osam.indosm.info_manager;

public class User {
    public String rank;
    public String name;
    public String UUID;
    public String logtime;
    public String intoWork;

    public User() {

    }

    public User(String QRMessage) {
        //Receiving {rank : ~~,  name : ~~, UUID : ~~, Time : ~~, intoWork : ~~}
        String Personal_info[] = {"","","","",""};
        for(int i=0;i<5;i++){
            Personal_info[i] = QRMessage.split(",")[i].split(": ")[1];
        }
        this.rank = Personal_info[0];
        this.name = Personal_info[1];
        this.UUID = Personal_info[2];
        this.logtime = Personal_info[3];
        this.intoWork = Personal_info[4];
    }

    public String Getrank(){ return this.rank;}
    public String Getname(){ return this.name;}
    public String GetUUID(){ return this.UUID;}
    public String Gettime(){ return this.logtime;}
    public String GetintoWork(){ return this.intoWork;}

}
