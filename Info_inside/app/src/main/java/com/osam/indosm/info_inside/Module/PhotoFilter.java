package com.osam.indosm.info_inside.Module;

import android.util.Log;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.content.BroadcastReceiver;
import android.os.IBinder;
import java.util.TimerTask;
import java.util.Timer;
import java.io.File;
import android.os.Environment;

public class PhotoFilter extends Service {
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent){
        }
    };
    private String StartingFilename;
    private String privateKey;
    boolean sw = true;
    TimerTask tt;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public IBinder onBind(Intent intent) {
        // Service 객체와 (화면단 Activity 사이에서)
        // 통신(데이터를 주고받을) 때 사용하는 메서드
        // 데이터를 전달할 필요가 없으면 return null;
        return null;
    }
    @Override
    public void onCreate(){
        super.onCreate();
        Log.d("Main","File Filtering Service Ready");
        sw = true;
        StartingFilename = getRecentFilename();
        Log.d("Main","Startfile : "+StartingFilename);
        startBroadcastReceiver();
        tt = new TimerTask(){
            @Override
            public void run(){
                if(sw) {
                    startBroadcastReceiver();
                }
            }
        };

        Timer timer = new Timer();
        timer.schedule(tt,0,30000);
    }
    public void onDestroy(){
        super.onDestroy();
        Log.d("Service","Destroy");
        sw = false;
        tt.cancel();
    }

    private void startBroadcastReceiver() {
        String fileName = getRecentFilename();
        Log.d("Main","RecentFile : "+fileName);
        if(!StartingFilename.equals(fileName)){
            Log.d("Main","----------Recent File is Different");
            File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera/" + fileName);
            file.renameTo(new File(Environment.getExternalStorageDirectory()+"/encryptec/"+fileName.substring(0,fileName.length()-3)+"ifei"));
            //AES256Chiper.encrypt(file, new File(Environment.getExternalStorageDirectory() + "/encrypted/"+fileName), privateKey);
            //AES256Chiper.decrypt(new File(Environment.getExternalStorageDirectory()+"/encrypted/"+fileName), new File(Environment.getExternalStorageDirectory()+"/decrypted/"+fileName), privateKey);
            if(file.delete()){
                Log.d("Main","File Deleted");
            }
            else{
                Log.d("Main","Why not Removed!!!1");
            }
        }

    }

    private void stopBroadcastReceiver() {
        unregisterReceiver(broadcastReceiver);
    }

    private String getRecentFilename(){
        String fileName = null;
        File[] listFiles = (new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera/").listFiles());
        int num = listFiles.length-1;
        if(listFiles[num].getName().endsWith("jpg") || listFiles[num].getName().endsWith(".bmp")){
            fileName = listFiles[num].getName();
            return fileName;
        }
        else{
            return null;
        }
    }
}
